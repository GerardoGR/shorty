# Shorty

URL shortner app made in Golang

## Requirements

- Golang 1.18.x
- Make

## Build

To build shorty run `make`. This will create the statically-linked binary `./shorty`.

## Run

After building shorty, run it by executing `./shorty`. The following cli parameters can be used:

* `-listen-address`: Address, in the form of `$HOST:$PORT`, where shorty will listen to. Defaults `localhost:8000`
* `-domain-name`: Domain name used. This affects how the shortened URLs are presented. Defaults `localhost:8000`

## Endpoints

While the application is running the following endpoints exist:

* `POST /api/v1/shorten/$URL_TO_SHORTEN` where `$URL_TO_SHORTEN` expects an *abbreviated URL* (see "Abbreviated URLs" in [RFC2396](https://www.ietf.org/rfc/rfc2396.txt)), e.g example.com/about.
  - If `$URL_TO_SHORTEN` is empty a `BadRequest 400 - URL cannot be empty` response is returned.
  - If `$URL_TO_SHORTEN` was submitted for the first time a new shortened-url returned `{"identifier":"$DOMAIN_NAME/$IDENTIFIER"}` where `$IDENTIFIER` can be used to do a lookup of the original URL (see `GET /api/v1/lookup/$IDENTIFIER`).
  - If `$URL_TO_SHORTEN` was already shortened, the existing shortened url will be returned. This is to avoid duplicated shortened URLs.

* `GET /api/v1/lookup/$IDENTIFIER` where `$IDENTIFIER` is the identifier generated in `/api/v1/shorten/$URL_TO_SHORTEN`.
  - If `$IDENTIFIER` exists a `{"url": "$URL"}` response is returned where `$URL` is the original unshortened URL.
  - If `$IDENTIFIER` doesn't match the regex `^[a-zA-Z0-9]{10}$` then a `BadRequest 400 - Identifier must match: ^[a-zA-Z0-9]{10}$` response is returned.
  - If `$IDENTIFIER` has not been generated then a `NotFound 404 - Identifier not found` response is returned.

## Usage

1. Create a new shortened URL:

```shell
curl -s -XPOST http://localhost:8000/api/v1/shorten/example.com/about
{"identifier":"localhost:8000/WD8F2qNfHK"}
```

2. Lookup the URL corresponding to the identifier:

```shell
curl -s http://localhost:8000/api/v1/lookup/WD8F2qNfHK
{"url":"example.com/about"}
```

## Rationale

- To simplify the input handling code in the `POST /api/v1/shorten/$URL_TO_SHORTEN` API endpoint *abbreviated URLs* were chosen. Due to the fact that URLs can contain the protocol (e.g `http://`) I would prefer to send in an `application/json` body the "URL", e.g `curl -XPOST -H "Content-Type: application/json -d {"url": "https://example.con"}`.
- Instead of defining as a CLI parameter the domina-name it is possible to infer it from within the server. The only drawback of that, is that in a production setup, the application would sit behind a load-balancer which might have a different domain-name than the application but that we would need to redirect the calls to. Therefor the `-domain-name` flag was implemented.
- The `-listen-address` flag was implemented so that while developing we just need to open up the application to the loopback network interface, and if the need be, like in a prodcution setup the application could listen to other network interfaces.
- To generate the shortened URLs a simple permutation algorithm was chosen because it has a high number of permutations, e.g [a-zA-Z0-9] contains 62 characters and to form the identifier we use 10 of those which translates to a 62^10 permutations.
- The application is assumed to be running standalone (e.g 1 single instance) this will perform pretty well given that collisions are highly unlikely and that all elements are mantained in memory.

## TODO

- [ ] Implement serialisation parameter to change them from JSON to any of: XML and CBOR. This can be achieved via an `Accept` header from the client request, e.g `curl -H "Accept: text/xml" ...`
- [ ] Redirect user to original URL when visiting `GET $DOMAIN_NAME/$IDENTIFIER`

## Development

Running shorty in the development mode can be done by running:

```shell
make develop
Starting server localhost:8000
```
