package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"regexp"
	"strings"
)

func main() {
	// TODO: Validate listenAddress: $host:$port
	listenAddress := flag.String("listen-address", "localhost:8000", "Domain name where shorty is being served")
	flag.Parse()

	http.HandleFunc("/api/v1/shorten/", shorten)
	http.HandleFunc("/api/v1/lookup/", lookup)

	fmt.Printf("Starting server %s\n", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}

// TODO: Validate domain-name
var DomainName *string = flag.String("domain-name", "localhost:8000", "Domain name where shorty is being served")

func shorten(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Only POST is supported", http.StatusMethodNotAllowed)
		return
	}

	// TODO: Validate url
	url := strings.TrimPrefix(r.URL.Path, "/api/v1/shorten/")
	if url == "" {
		http.Error(w, "URL cannot be empty", http.StatusBadRequest)
		return
	}

	urlIdentifier, knownUrl := URLToIdMapping[url]
	if !knownUrl {
		urlIdentifier = generateURLIdentifier()
		URLToIdMapping[url] = urlIdentifier
		IdToURLMapping[urlIdentifier] = url
	}

	response := ShortenResponse{
		Identifier: fmt.Sprintf("%s/%s", *DomainName, urlIdentifier),
	}
	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, "Unexpected server error", http.StatusInternalServerError)
		return
	}
}

func generateURLIdentifier() string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, 10)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

type ShortenResponse struct {
	Identifier string `json:"identifier"`
}

var URLToIdMapping map[string]string = map[string]string{}

func lookup(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Only GET is supported", http.StatusMethodNotAllowed)
		return
	}

	// Validate identifier
	identifier := strings.TrimPrefix(r.URL.Path, "/api/v1/lookup/")
	isValidIdentifier, err := regexp.Match(`^[a-zA-Z0-9]{10}$`, []byte(identifier))
	if !isValidIdentifier {
		http.Error(w, "Identifier must match: ^[a-zA-Z0-9]{10}$", http.StatusBadRequest)
		return
	}
	if err != nil {
		http.Error(w, "Unexpected server error", http.StatusInternalServerError)
		return
	}

	// Return URL
	url, knownIdentifier := IdToURLMapping[identifier]
	if !knownIdentifier {
		http.Error(w, "Identifier not found", http.StatusNotFound)
		return
	}

	response := LookupResponse{
		URL: url,
	}
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, "Unexpected server error", http.StatusInternalServerError)
		return
	}
}

type LookupResponse struct {
	URL string `json:"url"`
}

var IdToURLMapping map[string]string = map[string]string{}
