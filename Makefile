all: build

build: shorty

shorty: main.go
	CGO_ENABLED=0 go build -o shorty .

develop:
	@go run main.go
.PHONY: develop
